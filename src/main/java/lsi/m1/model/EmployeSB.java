/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lsi.m1.model;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import static lsi.m1.utils.Constantes.REQ_DELETE_EMPLOYE;
import static lsi.m1.utils.Constantes.REQ_INSERT_EMPLOYE;
import static lsi.m1.utils.Constantes.REQ_TOUS_EMPLOYES;
import static lsi.m1.utils.Constantes.REQ_UPDATE_EMPLOYE;

/**
 *
 * @author odran
 */
@Stateless
public class EmployeSB {

    @PersistenceContext(unitName = "com.JPA_V2_Maven_Java_war_1.0-SNAPSHOTPU")
    
    private EntityManager em;
    private List<Employes> listeEmployes;
  
    public List getEmployes(){
        Query q = em.createQuery(REQ_TOUS_EMPLOYES);
        List l = q.getResultList();
        return l ;
    }

    /**
     *
     * @param selectId
     */
    @Transactional
    //@SuppressWarnings("JPQLValidation")
    public void delete(String selectId){
        int temp = Integer.parseInt(selectId);
        
        Query q;   
        q = em.createNativeQuery(REQ_DELETE_EMPLOYE)
        .setParameter(1,temp);
        q.executeUpdate();
    }

    /**
     *
     * @param employes
     */
    @Transactional
    public void register(Employes employes){
        
        Query q;   
        q = em.createNativeQuery(REQ_INSERT_EMPLOYE)
				
	.setParameter(1,employes.getNom())
        .setParameter(2,employes.getPrenom())
        .setParameter(3,employes.getTeldom())
        .setParameter(4,employes.getTelport())
        .setParameter(5,employes.getTelpro())
        .setParameter(6,employes.getAdresse())
        .setParameter(7,employes.getCodepostal())
        .setParameter(8,employes.getVille())
        .setParameter(9,employes.getEmail());
        q.executeUpdate();
    }    

    /**
     *
     * @param employes
     */
    @Transactional
    public void update(Employes employes){
        Query q;
        q = em.createNativeQuery(REQ_UPDATE_EMPLOYE)       
        
        .setParameter(1,employes.getNom())
        .setParameter(2,employes.getPrenom())
        .setParameter(3,employes.getTeldom())
        .setParameter(4,employes.getTelport())
        .setParameter(5,employes.getTelpro())
        .setParameter(6,employes.getAdresse())
        .setParameter(7,employes.getCodepostal())
        .setParameter(8,employes.getVille())
        .setParameter(9,employes.getEmail())
        .setParameter(10,employes.getId());
        q.executeUpdate();
    }
    
    public Employes getEmployeById(String selectId){
        listeEmployes = getEmployes();  
        for (Employes employes : listeEmployes) {
            if(employes.getId().equals(Integer.parseInt(selectId))) {
                return employes;
            } 
        }
        return null;
    }    
}
