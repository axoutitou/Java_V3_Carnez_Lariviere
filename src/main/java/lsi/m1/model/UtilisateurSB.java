/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lsi.m1.model;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import static lsi.m1.utils.Constantes.REQ_UTILISATEURS;

/**
 *
 * @author odran
 */
@Stateless
public class UtilisateurSB {
  @PersistenceContext(unitName = "com.JPA_V2_Maven_Java_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    private List<Utilisateur> listeUsers;
  
    public List getUtilisateur(){
        Query q = em.createQuery(REQ_UTILISATEURS);
        List l = q.getResultList();
        return l ;
    }
    

    public boolean verifInfosConnexion(Utilisateur userInput) {
        boolean connexion = false;
        listeUsers =  getUtilisateur();
        for (Utilisateur userBase : listeUsers) {
                if (userBase.getLogin().equals(userInput.getLogin()) && userBase.getPassword().equals(userInput.getPassword())) {
                        connexion = true;
                }
        }
        return connexion;
    }
    
    
}
