/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lsi.m1.ctrl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import lsi.m1.model.Utilisateur;
import lsi.m1.model.UtilisateurSB;
import lsi.m1.model.Employes;
import lsi.m1.model.EmployeSB;
import static lsi.m1.utils.Constantes.*;


@WebServlet( name="Controleur", urlPatterns = "/Controleur" )
public class Controleur extends HttpServlet {

	  private HttpSession session;
	  
	  private Client client;
	  private WebTarget employesTarget;
	  private WebTarget usersTarget;
			  
	 /**
	  * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	  * methods.
	  *
	  * @param request servlet request
	  * @param response servlet response
	  * @throws ServletException if a servlet-specific error occurs
	  * @throws IOException if an I/O error occurs
	  */
	  public Controleur(){
		  client = ClientBuilder.newClient();
		  employesTarget = client.target(URL_EMPLOYES);
		  usersTarget = client.target(URL_UTILISATEURS);
	  }
	
	 protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                    
                    if(request.getParameter(FRM_BTN_CONNEXION) != null){
			   Utilisateur userInput = new Utilisateur();
	
			   if(request.getParameter(FRM_LOGIN).length() == 0 ||  request.getParameter(FRM_MDP).length() == 0) showAccueil(request, response, ERR_EMPTY_FIELD);
			   
			   else{
					userInput.setLogin(request.getParameter(FRM_LOGIN));
					userInput.setPassword(request.getParameter(FRM_MDP));
					
					if (verifInfosConnexion(userInput)) {
						session = request.getSession();
						session.setAttribute("user", userInput);
						showHome(request, response, null, null);
					} 
					else showAccueil(request, response, ERR_WRONG_LOGIN);						
			   }
                    }
		
		else if(request.getParameter(FRM_BTN_SUPPRIMER) != null){
			   String selectId = request.getParameter(FRM_SELECTED_EMPLOYE);
			   
			   if(selectId == null) showHome(request, response, "error", ERR_EMPTY_SUPPR);
			   else{
					deleteEmployeByIdRequest(selectId);
					showHome(request, response, "success", SUC_SUPPR);
			   }
                    }
					
		else if(request.getParameter(FRM_BTN_AJOUTER) != null || request.getParameter(FRM_BTN_MODIFIER) != null){
		        Employes employe = new Employes();
				
                           if (request.getParameter(FRM_BTN_MODIFIER) != null) employe.setId(request.getParameter(FRM_ID));
                           
                           if(request.getParameter(FRM_BTN_AJOUTER) != null)  employe.setId(1);
                           
			   employe.setNom(request.getParameter(FRM_NOM));
			   employe.setPrenom(request.getParameter(FRM_PRENOM));
			   employe.setTeldom(request.getParameter(FRM_TEL_DOM));
			   employe.setTelport(request.getParameter(FRM_TEL_PORT));
			   employe.setTelpro(request.getParameter(FRM_TEL_PRO));
			   employe.setAdresse(request.getParameter(FRM_ADRESSE));
			   employe.setCodepostal(request.getParameter(FRM_CODE_POSTAL));
			   employe.setVille(request.getParameter(FRM_VILLE));
			   employe.setEmail(request.getParameter(FRM_EMAIL));
			   
			   if(request.getParameter(FRM_BTN_AJOUTER) != null){
					postEmployeRequest(employe);
					showHome(request, response, "success", SUC_AJOUT);
			   }
			   else{
					putEmployeRequest(employe);
					showHome(request, response, "success", SUC_MODIF);
			   }                           
                    }
					
                    else if(request.getParameter(FRM_BTN_SHOW_DETAILS) != null){
			   String selectId = request.getParameter(FRM_SELECTED_EMPLOYE);
			   
			   if(selectId == null) showHome(request, response, "error", ERR_EMPTY_DETAILS);
			   else showDetailsUtilisateur(request, response, selectId);
			   
                    }
                    
                    else if(request.getParameter(FRM_BTN_SHOW_AJOUT) != null) showAjout(request, response);
                    
                    else if(request.getParameter(FRM_BTN_SHOW_LISTE) != null) showHome(request, response, null, null);
                    
                    else if(request.getParameter(FRM_BTN_DECONNEXION) != null){
			   session = request.getSession();
			   session.removeAttribute("user");
			   showGoodbye(request, response);
                    }
                    else showAccueil(request, response, null);
                    
                    
	 }	  

	 // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
	 /**
	  * Handles the HTTP <code>GET</code> method.
	  *
	  * @param request servlet request
	  * @param response servlet response
	  * @throws ServletException if a servlet-specific error occurs
	  * @throws IOException if an I/O error occurs
	  */
	 @Override
	 protected void doGet(HttpServletRequest request, HttpServletResponse response)
			 throws ServletException, IOException {
		  processRequest(request, response);
	 }

	 /**
	  * Handles the HTTP <code>POST</code> method.
	  *
	  * @param request servlet request
	  * @param response servlet response
	  * @throws ServletException if a servlet-specific error occurs
	  * @throws IOException if an I/O error occurs
	  */
	 @Override
	 protected void doPost(HttpServletRequest request, HttpServletResponse response)
			 throws ServletException, IOException {
		  processRequest(request, response);
	 }

	 /**
	  * Returns a short description of the servlet.
	  *
	  * @return a String containing servlet description
	  */
	 @Override
	 public String getServletInfo() {
		  return "Short description";
	 }// </editor-fold>
	 
	 private List<Employes> getAllEmployesRequest(){
		  List<Employes> listEmployes = employesTarget.request().get(new GenericType<List<Employes>>() {});
		  return listEmployes;
	 }
	 
	 private Employes getEmployeByIdRequest(String paramId){
		Employes employe = employesTarget.path(paramId).request().get(Employes.class);  
		return employe;
	 }
	 
	 private void deleteEmployeByIdRequest(String paramId){
		  employesTarget.path(paramId).request().delete();
	 }
	 
	 private void postEmployeRequest(Employes employe){
		  employesTarget.request(MediaType.APPLICATION_XML).post(Entity.entity(employe, MediaType.APPLICATION_XML));
	 }
	 
	 private void putEmployeRequest(Employes employe){
		  String employeId = employe.getId().toString();
		  employesTarget.path(employeId).request(MediaType.APPLICATION_XML).put(Entity.entity(employe, MediaType.APPLICATION_XML));
	 }

	 private List<Utilisateur> getAllUsersRequest(){
		  List<Utilisateur> listUsers = usersTarget.request().get(new GenericType<List<Utilisateur>>() {});
		  return listUsers;
	 }
		 
	 private boolean verifInfosConnexion(Utilisateur userInput){
		  boolean connexion = false;
		  List<Utilisateur> listUsers = getAllUsersRequest();
		  for (Utilisateur user : listUsers) {
			   if (user.getLogin().equals(userInput.getLogin()) && user.getPassword().equals(userInput.getPassword())) {
					connexion = true;
			}
		}
		  return connexion;
	 }
	 
	 private void showAccueil(HttpServletRequest request, HttpServletResponse response, String msg){
		request.setAttribute("error", msg);
		  try {
			   request.getRequestDispatcher(JSP_ACCUEIL).forward(request, response);
		  } catch (ServletException ex) {
			   Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
		  } catch (IOException ex) {
			   Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
		  }
	 }
	 
	 private void showHome(HttpServletRequest request, HttpServletResponse response, String type, String msg){
		if(type != null) request.setAttribute(type, msg);
		request.setAttribute("listEmployes", getAllEmployesRequest());
		  try {
			   request.getRequestDispatcher(JSP_USERS).forward(request, response);
		  } catch (ServletException ex) {
			   Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
		  } catch (IOException ex) {
			   Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
		  }
	 }
	 
	 private void showDetailsUtilisateur(HttpServletRequest request, HttpServletResponse response, String employeId){
		  request.setAttribute("employe", getEmployeByIdRequest(employeId));
		  try {
			   request.getRequestDispatcher(JSP_NEW).forward(request, response);
		  } catch (ServletException ex) {
			   Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
		  } catch (IOException ex) {
			   Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
		  }
	 }
	 
	 private void showAjout(HttpServletRequest request, HttpServletResponse response){
		  try {
			   request.getRequestDispatcher(JSP_NEW).forward(request, response);
		  } catch (ServletException ex) {
			   Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
		  } catch (IOException ex) {
			   Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
		  }
	 }
	 
	 private void showGoodbye(HttpServletRequest request, HttpServletResponse response){
		  try {
			   request.getRequestDispatcher(JSP_GOODBYE).forward(request, response);
		  } catch (ServletException ex) {
			   Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
		  } catch (IOException ex) {
			   Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
		  }
	 }
}
